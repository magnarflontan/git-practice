#include "calculator.h"

int Calculator::Add (double a, double b)
{
	return (int)(a + b + 0.5f);
}

int Calculator::Sub (double a, double b)
{
	return Add (a, -b);
}

int Calculator::Mul (double a, double b)
{
    return (int)(a * b + 0.5f);
}

int Calculator::Div (double a, double b)
{
	return (int)(a / b + 0.5f); 
}

long Calculator::Square (int a)
{
	long result;
	__asm__(
		"leal (%1,%1), %0\n"
		:"=r" (result)
		:"r" (a)
	);
	return result;
}